﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AppConfig
{
    public static int maxTouches = 5;
    public static int rotateDegree = 50;
    public static int freezeCoef = 4;
    public static int moveSpeed = 4;
    public static int coinRadius = 2;
}
