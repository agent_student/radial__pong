﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterScript : MonoBehaviour
{
    public GameController gameController;
    public CoinController coinController;

    public Rigidbody2D rb;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void SlowingDown()
    {
        rb.velocity /= AppConfig.freezeCoef;
    }

    public void ReturnNormalSpeed()
    {
        rb.velocity *= AppConfig.freezeCoef;
    }

    public void SetStartBallValues()
    {
        rb.velocity = Vector2.up * AppConfig.moveSpeed;
    }

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Coin")
		{
			gameController.IncreaseCoins();
			coinController.ChangeCoinPosition();
		}
	}
}
