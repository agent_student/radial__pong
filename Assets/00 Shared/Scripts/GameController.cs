﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public CharacterScript characterScript;
	public BlocksController blocksController;
    public CoinController coinController;

	public Text scoreText, moneyText;
    public GameObject startAgain;
    public int touchesCount, scoreCount;

	int moneyCount;
	bool freezing, enableFreezing;

    void Start () 
    {
        SetStartValues();
	}
	
	void Update () {
        if (Input.GetMouseButton(0) && enableFreezing == true)
			Freeze();
		else if (freezing == true) Unfreeze();
	}

    public void IncreaseCoins() 
    {
        moneyCount++;
        moneyText.text = moneyCount.ToString();
    }

    public void IncreaseTouches() 
    {
        touchesCount++;
    }

    public void GameOver()
    {
        enableFreezing = false;
        characterScript.gameObject.transform.position = Vector2.zero;
        characterScript.rb.velocity = Vector2.zero;
        startAgain.SetActive(true);
    }

	public void StartAgainButtonClick()
	{
		blocksController.SetStartState();
		characterScript.SetStartBallValues();
		SetStartValues();
	}

	private void Freeze()
	{
        if (freezing == false)
        {
            characterScript.SlowingDown();
        }
		freezing = true;
		blocksController.Rotation();
	}

	private void Unfreeze()
	{
        characterScript.ReturnNormalSpeed();
		freezing = false;
	}

    private void SetStartValues() 
    {
        scoreCount = 0;
        moneyCount = 0;
        touchesCount = 0;
        scoreText.text = scoreCount.ToString();
        moneyText.text = moneyCount.ToString();
		freezing = false;
        enableFreezing = true;
		startAgain.SetActive(false);
		characterScript.SetStartBallValues();
		coinController.ChangeCoinPosition();
    }
}
