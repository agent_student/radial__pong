﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour {

    public GameController gameController;
    public BlocksController blocksController;

    public int ticks;

    int oldScore;

	void Awake () 
    {
        ticks = 0;
	}
	
	void Update () 
    {
        
	}

    public void SetColorFree()
    {
        this.gameObject.GetComponent<Renderer>().material.color = Color.white;
        this.gameObject.tag = "FreeBlock";
    }

    private void SetColorBusy() 
    {
        this.gameObject.GetComponent<Renderer>().material.color = Color.red;
        this.gameObject.tag = "BusyBlock";
    }

	private void OnCollisionEnter2D(Collision2D other)
	{
        if (other.gameObject.tag == "Player")
        {
            gameController.IncreaseTouches();
            if (gameController.touchesCount < (ticks + AppConfig.maxTouches + 1) && this.gameObject.tag == "BusyBlock")
            {
                gameController.GameOver();
            }
            else
            {
				blocksController.CheckAllBlocks();
				SetColorBusy();
                ticks = gameController.touchesCount;
                gameController.scoreCount = gameController.touchesCount;
                gameController.scoreText.text = gameController.scoreCount.ToString();
            }
        }
	}
}
