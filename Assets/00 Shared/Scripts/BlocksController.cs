﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocksController : MonoBehaviour {

	public List<BlockController> blockController;
    public GameController gameController;

	void Start () {
		
	}

	void Update () {
		
	}

    public void SetStartState()
    {
        this.transform.rotation = Quaternion.Euler(0, 0, 0);
		foreach (var block in blockController)
		{
			block.SetColorFree();
		}     
    }

	public void Rotation()
    {
        this.transform.Rotate(new Vector3(0, 0, -AppConfig.rotateDegree) * Time.deltaTime);
    }

    public void StopRotation()
    {
        this.transform.Rotate(Vector3.zero);
    }

    public void CheckAllBlocks() 
    {
		foreach (var block in blockController)
		{
            if (gameController.touchesCount >= (block.ticks + AppConfig.maxTouches) && block.tag == "BusyBlock")
			{
				block.SetColorFree();
			}
		}
	}
}
