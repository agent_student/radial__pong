﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour {

	void Start () {
		
	}
	
	void Update () {
		
	}

	public void ChangeCoinPosition()
	{
        this.gameObject.transform.position = Random.insideUnitCircle * AppConfig.coinRadius;
	}
}
