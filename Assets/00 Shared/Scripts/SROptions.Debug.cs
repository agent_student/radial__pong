﻿using System.ComponentModel;

public partial class SROptions

{
    [Category("TouchesCount")]
    public int maxTouches {
        get { return AppConfig.maxTouches; }
        set { AppConfig.maxTouches = value; }
	}

	[Category("BlocksRotateDegree")]
	public int rotateDegree
	{
		get { return AppConfig.rotateDegree; }
		set { AppConfig.rotateDegree = value; }
	}

    [Category("BallFreezeCoefficient")]
    public int freezeCoef
	{
		get { return AppConfig.freezeCoef; }
		set { AppConfig.freezeCoef = value; }
	}

	[Category("BallMoveSpeed")]
	public int moveSpeed
	{
		get { return AppConfig.moveSpeed; }
		set { AppConfig.moveSpeed = value; }
	}

	[Category("CoinSpawnRadius")]
	public int coinRadius
	{
		get { return AppConfig.coinRadius; }
		set { AppConfig.coinRadius = value; }
	}
}
